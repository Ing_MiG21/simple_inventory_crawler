<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\WishList;
use Goutte\Client;

 /**
  * @author Yrvin Escorihuela
  * 
  */

class TestCrawlerController extends Controller
{
    /**
    * Class constructor
    *
    */
    public function __construct()
    {
        
    }

    /**
    * Checks if the url is available
    * @param string 
    * @return response
    */
    public static function checkUrl($url)
    {
            $response = \Curl::to($url)->returnResponseObject()->withTimeout(10)->get();            
            return $response;
    }
    /**
    * Return a hardcoded url for crawl
    * @param string
    * @return string
    */
    public static function getUrlByKey($order)
    {
        $product_sort = [ 
                        'asc'   => 'https://www.appliancesdelivered.ie/search',
                        // 'asc'    => 'https://www.XXXX.ie/search',
                        // 'desc'   => 'https://www.appXXXXliancesdelivered.ie/search?sort=price_desc'
                        'desc'  => 'https://www.appliancesdelivered.ie/search?sort=price_desc'
                    ];      
        if (!array_key_exists($order, $product_sort))
            die("Invalid sorting key");
        return $product_sort[$order];
    }

    /**
    * Gets the needed HTML elements for build the new view
    * @param string
    * @param integer
    * @return array
    */
    public static function checkCrawler($order, $qty_products = null)
    {
        // $crawler = \Goutte::request('GET', 'https://www.appliancesdelivered.ie/search?sort=price_desc');
        // $crawler = \Goutte::request('GET', 'https://www.appliancesdelivered.ie/search');
        $counter_loop_flag = 0;
        $element_per_page = 10;
        if ($qty_products != null && $qty_products > 10)
            $element_per_page = $qty_products;

        $crawler = \Goutte::request('GET', self::getUrlByKey($order));

        $result_row = new \DOMDocument();       

        $products = [];
        
        $crawler->filter('.search-results-product')->each(function ($node, $i) use ($result_row, &$products, $element_per_page) {   

            if ($i > ($element_per_page - 1))
                return;

            // It read the HTML in the required node and just works over that,
            // in this case, only the products containers
            $result_row->loadHTML($node->html()); 

            $data = $result_row->getElementsByTagName('div');

            // Get the first div: index 0 which contents the product image
            $first_div = $data->item(0);

            // Get the second div: index 1 which contents the brand logo and product title
            $second_div = $data->item(1);

            // Get the third div: index 2 which contents the price, button for buy and associate services
            $third_div = $data->item(2);

            // Get the first image of the row
            $product_image_route = $first_div->getElementsByTagName('img')->item(0)->getAttribute('src');

            // Get the logo brand
            $logo_brand = $second_div->getElementsByTagName('img')->item(0)!= null ? $second_div->getElementsByTagName('img')->item(0)->getAttribute('src'):'';

            // Get the product header
            $product_header = $second_div->getElementsByTagName('h4')->item(0);

            // Get the product link 
            $product_link = $product_header->getElementsByTagName('a')->item(0)->nodeValue;

            // Get the product description
            $product_description = $second_div->getElementsByTagName('ul')->item(0);

            // Get the product price
            $product_price = $third_div->getElementsByTagName('div')->item(1) != null? $third_div->getElementsByTagName('div')->item(1)->getElementsByTagName('h3')->item(0):null;

            /**
            * Get the element id saved in the database
            */
            // At first time we need get the action form in this step
            $product_form_action = $third_div->getElementsByTagName('form')->item(0)->getAttribute('action');
            
            // After we need explode the action url by slash character
            $product_url_array = explode("/",$product_form_action);

            // Take the last element of this array 
            $product_id = $product_url_array[count($product_url_array) - 1];


            $learn_more = $third_div->getElementsByTagName('div')->item(1)->getElementsByTagName('a')->item(0);

            // Store all we need in an array and pass it to view or caching system
            $final_data = [ 'product_id'            =>  $product_id,
                            'image_product'         =>  $product_image_route,
                            'logo_brand'            =>  $logo_brand != '' ? $logo_brand : null,
                            'product_title'         =>  $product_link,
                            'product_description'   =>  $product_description->ownerDocument->saveHTML($product_description),
                            'product_price'         =>  $product_price != null? str_replace(utf8_encode('€'),'',$product_price->nodeValue):'Not gathered',
                            'product_learn_more'    =>  $learn_more != null? $learn_more->ownerDocument->saveHTML($learn_more):''
                            ];

            $products[] = $final_data;  

        });
        $products['sort'] = $order;

        return $products;
    }

    /**
    * Iterates over an array with all the pages of appliancesdelivered for create
    * a general cached list of products
    *
    * @param array
    * @return mixed array
    */
    public static function gatherAllPages($array_pages, $qty_products = null)
    {
        $element_per_page = 10;
        if ($qty_products != null && $qty_products > 10)
            $element_per_page = $qty_products;

        $result_row = new \DOMDocument();       

        $products = [];
    
        foreach ($array_pages as $page) {
            $crawler = \Goutte::request('GET', $page);
            $crawler->filter('.search-results-product')->each(function ($node, $i) use ($result_row, &$products, $element_per_page)
            {
                if ($i > ($element_per_page - 1))
                    return;
                
                $result_row->loadHTML($node->html()); // 

                $data = $result_row->getElementsByTagName('div');

                // Get the first div: index 0 which contents the product image
                $first_div = $data->item(0);

                // Get the second div: index 1 which contents the brand logo and product title
                $second_div = $data->item(1);

                // Get the third div: index 2 which contents the price, button for buy and associate services
                $third_div = $data->item(2);

                // Get the first image of the row
                $product_image_route = $first_div->getElementsByTagName('img')->item(0)->getAttribute('src');

                // Get the logo brand
                $logo_brand = $second_div->getElementsByTagName('img')->item(0)!= null ? $second_div->getElementsByTagName('img')->item(0)->getAttribute('src'):'';

                // Get the product header
                $product_header = $second_div->getElementsByTagName('h4')->item(0);

                // Get the product link 
                $product_link = $product_header->getElementsByTagName('a')->item(0)->nodeValue;

                // Get the product description
                $product_description = $second_div->getElementsByTagName('ul')->item(0);

                // Get the product price
                $product_price = $third_div->getElementsByTagName('div')->item(1) != null? $third_div->getElementsByTagName('div')->item(1)->getElementsByTagName('h3')->item(0):null;

                /*
                * Get the element id saved in the database
                */
                // At first time we need get the action form in this step
                $product_form_action = $third_div->getElementsByTagName('form')->item(0)->getAttribute('action');
                
                // After we need explode the action url by slash character
                $product_url_array = explode("/",$product_form_action);

                // Take the last element of this array 
                $product_id = $product_url_array[count($product_url_array) - 1];


                $learn_more = $third_div->getElementsByTagName('div')->item(1)->getElementsByTagName('a')->item(0);

                // Store all we need in an array and pass it to view or caching system
                $final_data = [ 'product_id'            =>  $product_id,
                                'image_product'         =>  $product_image_route,
                                'logo_brand'            =>  $logo_brand != '' ? $logo_brand : null,
                                'product_title'         =>  $product_link,
                                'product_description'   =>  $product_description->ownerDocument->saveHTML($product_description),
                                'product_price'         =>  $product_price != null? str_replace(utf8_encode('€'),'',$product_price->nodeValue):'Not gathered',
                                'product_learn_more'    =>  $learn_more != null? $learn_more->ownerDocument->saveHTML($learn_more):''
                                ];               

                $products[] = $final_data;  

            });
        }


        return $products;
    }

    /**
    * After crawl the page, add the selected content to cache
    * @param string
    * @return array 
    */

    public static function cachingProductsBySorting($order)
    {
        $value = \Cache::remember('products_'.$order, 25, function () use ($order) {
            $response = TestCrawlerController::checkUrl(TestCrawlerController::getUrlByKey($order));
            if ($response->status != 200) {
                return response()->json(['message' => $response->error], 503);
            }
            return TestCrawlerController::checkCrawler($order, 10);
        });     
        return $value;
    }
    /**
    * Get products and show the view depends the order
    * @param string
    * @return view 
    */
    public function getProductsBySorting($order)
    {
        $products = self::cachingProductsBySorting($order);
        return view('user.inventory')->with(['products' => $products]);
    }


    /**
    * Get all the crawled products without make sense of sorting order
    * @param array
    * @return mixed array
    */

    public static function cachingAllProducts($array_pages)
    {
        $value = \Cache::remember('products', 25, function () use ($array_pages) {
            foreach ($array_pages as $page) {
                $response = TestCrawlerController::checkUrl($page);
                if ($response->status != 200) {
                    return response()->json(['message' => $response->error], 503);
                }
            }
            return TestCrawlerController::gatherAllPages($array_pages, 10);
        });     
        return $value;      
    }

    /**
    * An abstraction of the previous function, only for pass the array of urls needed
    * @param array
    * @return void
    */

    public static function setAllProductsInCache($array_pages = null)
    {
        if ($array_pages == null) {
            $pages = ['asc' => 'https://www.appliancesdelivered.ie/search', 'desc'  => 'https://www.appliancesdelivered.ie/search?sort=price_desc'];
        } else {
            $pages = $array_pages;
        }
        self::cachingAllProducts($pages);           
    }    

}
