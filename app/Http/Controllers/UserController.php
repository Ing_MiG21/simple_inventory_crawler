<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Requests\UserProfileRequest;
use App\Http\Requests\UserPasswordRequest;
use App\User;

/**
*@author Yrvin Escorihuela
*
*/

class UserController extends Controller
{
    /**
    * Class constructor, with authentication middleware associated
    *
    */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
    * Show the view for edit name and email of the current authenticated user
    * @return view
    */
    public function editProfile()
    {
        $current_user = User::find(\Auth::id());        
        return view('user.profile')->with(['user' => $current_user]);
    }

    /**
    * Update in the model the data of the current user
    * @return view 
    */

    public function updateProfile(UserProfileRequest $request)
    {
        $updated_user = User::find(\Auth::id());
        $updated_user->name = $request->name;
        $updated_user->email = $request->email;
        $updated_user->save();
        flash('Your profile has been updated successfully',"success");
        return redirect()->back()->with(['user' => $updated_user]);

    }
    /**
    * Show the view for change password of the current authenticated user
    *
    * @return view
    */
    public function editPassword()
    {
        return view('user.change_password');
    }

    /**
    * Update the password of the current user
    * @return view
    */
    public function updatePassword(UserPasswordRequest $request)
    {
        $current_user = User::find(\Auth::id());
        if (\Hash::check($request->old_password, $current_user->password)) {
            $current_user->password = bcrypt($request->password);
            $current_user->save();
            flash('Your password has been updated successfully', 'success');
            return redirect()->back();
        }
        flash('Your old password doesn\'t match!!! ', 'danger');
        return redirect()->back();      
    }
}
