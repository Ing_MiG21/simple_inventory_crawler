<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\WishList;
use App\User;

/**
* @author Yrvin Escorihuela
*
*/

class WishListController extends Controller
{
    
    /**
    * Class constructor
    *
    */

    public function __construct()
    {
        $this->middleware('auth');        
        TestCrawlerController::setAllProductsInCache(null);
    }


    /**
    * Return the authenticated user's wishlist
    *
    * @return array
    */

    public function getWishList()
    {   
        $product_list_db = WishList::getListProducts();        
        $cached_products = \Cache::get('products');
        $product_list_array = [];

        while (list($key, $product) = each($product_list_db)) {
           foreach ($cached_products as $p) {
                if ($p['product_id'] == $product['product_id']) {
                    $p['added_at'] = date("m-d-Y", strtotime($product['created_at']))." at ".date("H:i:s", strtotime($product['created_at']));
                    $product_list_array[] = $p; 
                }
           }
        }
        return $product_list_array;
    }

    /**
    * Show the view of the wishlist 
    *
    *
    */

    public function showWishList()
    {
        return view('user.wishlist')->with(['products' => $this->getWishList()]);
    }


    /**
    * Add product in the user's wishlist
    * @param integer
    * @return void
    */

    public function addToWishList($product_id)
    {        
        $product = WishList::firstOrCreate(['product_id' => $product_id, 'user_id' => \Auth::id()]);
        if ($product->wasRecentlyCreated == true) {
            flash('The product ID# '.$product_id.' was added to my Wishlist',"success");
        } else {
            flash('The product ID# '.$product_id.' already exists in your Wishlist',"warning");
        }
        // return view('user.wishlist')->with(['products' => $this->getWishList()]);
        return redirect()->back();
    }

    /**
    * Remove a product from user's wishlist
    * @param integer
    * @return void
    */

    public function removeFromWishList($product_id)
    {
        $product = WishList::where('product_id', $product_id)->delete();
        if ($product == 1) {
            flash('The product ID# '.$product_id.' was delete from my Wishlist successfully',"success");
        } else {
            flash('The product ID# '.$product_id.' doesn\'t exist ',"danger");
        }
        return redirect()->back();
    }

}
