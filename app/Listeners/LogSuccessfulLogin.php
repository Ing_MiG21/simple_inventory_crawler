<?php

namespace App\Listeners;

use Illuminate\Auth\Events\Login;

use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Http\Controllers;
use App\Http\Controllers\TestCrawlerController;
class LogSuccessfulLogin
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  Login  $event
     * @return void
     */
    public function handle(Login $event)
    {
        TestCrawlerController::cachingProductsBySorting('asc');
        TestCrawlerController::cachingProductsBySorting('desc');
        TestCrawlerController::setAllProductsInCache();
    }
}
