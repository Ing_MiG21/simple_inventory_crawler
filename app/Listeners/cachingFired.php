<?php

namespace App\Listeners;

use App\Events\cachingAtFirstTime;
use App\Http\Controllers;
use App\Http\Controllers\TestCrawlerController;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
class cachingFired
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  cachingAtFirstTime  $event
     * @return void
     */
    public function handle(cachingAtFirstTime $event)
    {
        TestCrawlerController::cachingProductsBySorting('asc');
        TestCrawlerController::cachingProductsBySorting('desc');
        TestCrawlerController::setAllProductsInCache();
    }
}
