<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WishList extends Model
{
    //
    /**
    * The name of the table
    * @var string
    */
    protected $table = 'wishes_lists';
    /**
    * The attributes used for massive assignaments
    * @var array
    */    
    protected $fillable = ['product_id', 'user_id'];

    /**
    * Create relationship from many to one
    * @return class
    */
    public function User()
    {
        return $this->belongsTo(App\User::class);
    }

    /**
    * Return all the products in the wishlist
    * 
    * @return array    
    */
    public static function getListProducts()
    {
        $wish_list = self::where('user_id', \Auth::id())->orderBy('created_at', 'desc')->get();
        return $wish_list->toArray();
    }
}
