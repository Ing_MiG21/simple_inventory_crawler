# Simple Inventory Crawler
## Introduction
This project was built in Laravel 5.3 running at Homestead virtual machine with PHP 7.1 and Nginx web server I used Redis for the caching system, if you need more details about, you could review the composer.json inside the project.

### Setting up the project:

#### 1. Install the dependencies with composer (Dependency Manager for PHP): 
You must run ``` composer install ``` in the project root folder... It takes awhile.
#### 2. Check the Redis service in your machine:
You could do this by ```ps -ef | grep redis``` bash commands, if Redis is installed and active, you must this output like the highlighted: 
![Redis service](https://lh4.googleusercontent.com/UeIw45YHN2fad91oHPhtJ3qdg6ilkhIVISz4VqmqrnYIqPThYWlvEQRnRMEusHz4VfZmh29dQ_xrXQ=w1366-h584 "Redis")
Otherwise, you must install with the command
 ```$ sudo apt-get install redis-server ``` 

#### 3. Install predis for PHP and Laravel:
Inside the project root folder, run the command:
``` composer require predis/predis ```
This is enough to be able to connect Redis and Laravel now. The only thing left is changing CACHE_DRIVER key in .env into redis. Default port should be fine, if you modified that port just update it in config/cache.php. 

#### 4. Config database engine:
I used MySQL for this project, you must set  in you .env file, the params for connect to DB, You could do this in ```config/database.php``` file too. 

#### 5. Run migrations:
In the command line, use the command ``` php artisan migrate ``` inside the root project path, it will take a little bit, will create the tables in the database configurated previously.

#### 6. Run seeder
In order of make tests: login, logout, add products to wishlist, remove products to wishlist, change profile and password, you must run the seeder with the following command: 
``` php artisan db:seed --class=UserTableSeeder ```
It will create dummy data, you must set the name of class which contents the data generator (I used faker for this).

#### 7. Testing the application:
You can check the app with the data supplied by the seeder, all the mails (users) created were made with "123456" password, there's no problem.