<nav class="navbar navbar-default navbar-fixed-top">
    <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="/">Simple Inventory Crawler</a>
        </div>
        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"> <i class="fa fa-tasks" aria-hidden="true"></i>&nbsp; &nbsp; Actions <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="/test_view_crawler/asc"> <i class="fa fa-sort-amount-asc" aria-hidden="true"></i> &nbsp; &nbsp; Ascending order</a></li>
                        <li><a href="/test_view_crawler/desc"> <i class="fa fa-sort-amount-desc" aria-hidden="true"></i> &nbsp; &nbsp; Descending order</a></li>            
                    </ul>
                </li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <li><a href="{{ route('auth.wishlist') }} "> <i class="fa fa-heart" aria-hidden="true"></i> My Wishlist</a></li>
                    @if(Auth::check() == TRUE)
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"> <i class="fa fa-user-circle-o" aria-hidden="true"></i> {{ Auth::user()->name}} <span class="caret"></span></a>
                                <ul class="dropdown-menu">
                                    <li><a href="/edit_profile">Edit my profile</a></li>
                                    <li><a href="/edit_password">Change my password</a></li>
                                    <li role="separator" class="divider"></li>
                                    <li><a href="/logout">Logout</a></li>
                                </ul>
                        </li>
                    @else
                        <li><a href="/login"> <i class="fa fa-unlock" aria-hidden="true"></i> Login </a></li>          
                    @endif        
            </ul>
        </div><!-- /.navbar-collapse -->
    </div>
</nav>