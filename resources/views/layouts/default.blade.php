<!DOCTYPE html>
    <html>
        <head>
            <!-- This includes metatags and CSS libraries -->
            @include('includes.head')
        </head>
        <body>
        <!-- This includes the navbar  -->
        <header id="top_bar">
            @include('includes.header')      
        </header><!-- /header -->
        <!-- Container embedded view -->
        <div class="container margin_top_container">
            @yield('content') 
        <!-- This includes all the javascript files as jquery and bootstrap libraries  -->
        </div>
            @include('includes.footer')
        </body>
    </html>