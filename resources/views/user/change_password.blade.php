@extends('layouts.default')
@section('content')
    <h1>
        <strong>My profile's password</strong>
        <br>
    </h1>
    <div class="col-xs-12 col-md-offset-3 col-sm-12 col-md-6 col-lg-6">
        <div>
            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif      
        </div>
        <div>
            @include('flash::message')
        </div>  
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h3 class="panel-title">
                    <strong><i class="fa fa-pencil" aria-hidden="true"></i> Edit my password </strong>
                </h3>
            </div>
            <div class="panel-body">
                {!! Form::open(['route' => 'user.update_password', 'method' => 'post']) !!}
                    <div class="form-group">
                    <label for="name">Old password</label>          
                    {!! Form::password('old_password', ['class' => 'form-control', 'placeholder' => 'Old password', 'id' => 'old_password']) !!}
                    </div>
                    <div class="form-group">
                        <label for="name">My new password</label>           
                        {!! Form::password('password', ['class' => 'form-control', 'placeholder' => 'My new password', 'id' => 'new_password']) !!}
                    </div>
                    <div class="form-group">
                        <label for="name">Confirm my new password</label>           
                        {!! Form::password('password_confirmation', ['class' => 'form-control', 'placeholder' => 'Confirm my new password', 'id' => 'new_password_confirmation']) !!}
                    </div>
                    <button type="submit" class="btn btn-success"><strong>Save changes</strong> </button>         
                {!! Form::close() !!}
            </div>
        </div>
    </div>  
@stop