@extends('layouts.default')
@section('content')
    <div class="padding_top_section">    
        <h1>
            <strong><div>Current Inventory</div></strong>
            <br>
        </h1>
        <div>
            @include('flash::message')
        </div>  
        @foreach($products as $product)
            @if(is_array($product))
                <div class="panel panel-default">
                      <div class="panel-heading">
                        <h3 class="panel-title"> <strong> Product ID# {{ $product['product_id'] }} </strong></h3>
                      </div>
                    <div class="panel-body">
                        <div class="col-sm-12 col-md-3 col-lg-3">
                            <img src="{{ $product['image_product'] }} " class="img-responsive" alt="">  
                        </div>              
                        <div class="col-sm-12 col-md-9 col-lg-9">
                            <div class="media">
                                <div class="media-left">
                                    <img class="media-object" src="{{ $product['logo_brand'] }}" >
                                </div>
                                <div class="media-body">
                                    <h2 class="media-heading">
                                        {{ $product['product_title'] }}
                                    </h2>                          
                                    <h3 class="price">
                                        Price: € {{$product['product_price']}} &nbsp;<a href="/add_to_wishlist/{{$product['product_id']}}" title="Add to my wishlist" ><i class="fa fa-heart-o" aria-hidden="true"></i></a>
                                    </h3>
                                    <h5 class="price">
                                        {!! $product['product_description'] !!}     
                                    </h5>
                                    <h6>
                                        <strong>{!! $product['product_learn_more'] !!}</strong>
                                    </h6>
                                </div>                      
                            </div>                                      
                        </div>
                    </div>
                </div>
            @endif
        @endforeach
        @if(count($products) > 1)
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center">
                <h3><strong><a href="#" title="Back to top" onclick="$('html, body').animate({ scrollTop: 0 }, 'fast');">Back to top</a></strong></h3>
                <br>
                <br>                
            </div>
        @endif
    </div>
@stop