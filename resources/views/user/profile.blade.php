@extends('layouts.default')
@section('content')
    <h1>
        <strong>My Profile</strong>
        <br>
    </h1>
    <div class="col-xs-12 col-md-offset-3 col-sm-12 col-md-6 col-lg-6">
        <div>
            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif      
        </div>
        <div>
            @include('flash::message')
        </div>  
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h3 class="panel-title">
                    <strong><i class="fa fa-pencil" aria-hidden="true"></i> Edit my profile </strong>
                </h3>
            </div>
            <div class="panel-body">
            {!! Form::open(['route' => 'user.update_profile', 'method' => 'post']) !!}
                <div class="form-group">
                    <label for="name">Name</label>          
                    {!! Form::text('name',$user->name, $attributes = ['class' => 'form-control', 'placeholder' => 'User name', 'id' => 'name']) !!}
                </div>        
                <div class="form-group">
                    <label for="email">Email address</label>            
                    {!! Form::email('email',$user->email, $attributes = ['class' => 'form-control', 'placeholder' => 'user@mail.com', 'id' => 'email']) !!}
                </div>
                <button type="submit" class="btn btn-success"><strong>Save changes</strong> </button>         
            {!! Form::close() !!}
            </div>
        </div>
    </div>  
@stop