@extends('layouts.default')
@section('content')
    <div class="flex-center position-ref full-height">
        <div class="content">
            <div class="title m-b-md">
                @if(Auth::check())
                    <span class="welcome">Welcome to My Application</span> 
                @else
                    <span class="welcome">Login and create your own wishlist</span> 
                @endif
            </div>
        </div>
    </div>  
@stop