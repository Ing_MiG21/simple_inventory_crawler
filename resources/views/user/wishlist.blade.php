@extends('layouts.default')
@section('content')
<div class="padding_top_section">
    @if(empty($products))
        <h1>
            <strong><div>Your Wishlist is empty...</div></strong>
            <br>
        </h1      
    @else
        <h1>
            <strong><div>My Wishlist</div></strong>
            <br>
        </h1>
        <div>
            @include('flash::message')
        </div>      
        @foreach($products as $product)
            <div class="panel panel-default">
              <div class="panel-heading">
                <h3 class="panel-title"> <strong> Product ID# {{ $product['product_id'] }} added at {{  $product['added_at'] }}</strong> </h3>
              </div>
              <div class="panel-body">
                <div class="col-sm-12 col-md-3 col-lg-3">
                    <img src="{{ $product['image_product'] }} " class="img-responsive" alt="">  
                </div>              
                <div class="col-sm-12 col-md-9 col-lg-9">
                    <div class="media">
                        <div class="media-left">
                            <img class="media-object" src="{{ $product['logo_brand'] }}" >
                        </div>
                        <div class="media-body">
                            <h2 class="media-heading">{{ $product['product_title'] }}</h2>                          
                                <h3 class="price">
                                    Price: € {{$product['product_price']}} &nbsp;<a href="/del_from_wishlist/{{$product['product_id']}}" title="Delete from my wishlist" ><i class="fa fa-times delete" aria-hidden="true"></i></a>
                                </h3>
                                <h5 class="price">
                                    {!! $product['product_description'] !!}     
                                </h5>
                                <h6>
                                    <strong>{!! $product['product_learn_more'] !!}</strong>
                                </h6>
                        </div>                      
                    </div>                                      
                </div>
              </div>
            </div>
        @endforeach
        @if(count($products) > 1)
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center">
            <h3><strong><a href="#" title="Back to top" onclick="$('html, body').animate({ scrollTop: 0 }, 'fast');">Back to top</a></strong></h3>
            <br>
            <br>                
        </div>
        @endif
    @endif
</div>    
@stop