<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



/**
* @author: Yrvin Escorihuela
* 
*/
Route::get('/', function () {
    Event::fire(new App\Events\cachingAtFirstTime());
    return view('user.welcome');
});
/**
* In this route we can see how we want the products by sorting order
* 
*/
Route::get('test_view_crawler/{order}', ['as' => 'test.crawler',    'uses' => 'TestCrawlerController@getProductsBySorting']);

/**
* This group of route only for registered and logged users
*
*/
Route::group(['middleware' => ['web']], function() {
            Route::get('wishlist',                          ['as' => 'auth.wishlist',           'uses' => 'WishListController@showWishList']);
            Route::get('add_to_wishlist/{product_id}',      ['as' => 'auth.add_to_wishlist',    'uses' => 'WishListController@addToWishList'])->where('product_id', '[0-9]+');
            Route::get('del_from_wishlist/{product_id}',    ['as' => 'auth.add_to_wishlist',    'uses' => 'WishListController@removeFromWishList'])->where('product_id', '[0-9]+');
            Route::get('edit_profile',                      ['as' => 'user.edit_profile',       'uses' => 'UserController@editProfile']);
            Route::post('update_profile',                   ['as' => 'user.update_profile',     'uses' => 'UserController@updateProfile']);
            Route::post('update_password',                  ['as' => 'user.update_password',    'uses' => 'UserController@updatePassword']);            
            Route::get('edit_password',                     ['as' => 'user.edit_password',      'uses' => 'UserController@editPassword']);
            Route::get('logout',                            ['as' => 'auth.logout',             'uses' => 'Auth\LoginController@logout']);
            Auth::routes();         
    });

Route::get('/home', ['uses' => 'HomeController@index']);